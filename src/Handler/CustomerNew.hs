{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Handler.CustomerNew where 

import Import 
import Data.Text(Text)
import Yesod.Form.Types 

data Person = Person
    { personName          :: Text
    , personAddress       :: Text
    , personZipCode       :: Text
    , personCity          :: Text
    }
    deriving Show

data Join = Join {  joinFullName :: Text} deriving Show

joinForm :: Html -> MForm Handler (FormResult Join, Widget)
joinForm extra = do
  let
    fullNameDef = areq
                    (checkBool
                      ((>0) . length)
                      ("You must include your full, legal name" :: Text)
                      textField)
                    "Full Name"
                    Nothing

  (fullName, fullNameWidget) <- renderDivs fullNameDef mempty

  let
    widget =
      [whamlet|
              ^{extra}
              <p.my-4> another sep
              ^{fullNameWidget}
              |]

    joinRes = Join <$> fullName

  return (joinRes, widget)


getCustomerNewR2 :: Handler Html
getCustomerNewR2  = do
    ((res, widget), enctype) <- runFormGet joinForm
    defaultLayout
        [whamlet|
            <p>Result: #{show res}
            <form enctype=#{enctype}>
                ^{widget}
        |] 
