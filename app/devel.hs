{-# LANGUAGE PackageImports #-}
import "payme" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
